#!/bin/bash



#Pull artificat archive file from last build
wget https://gitlab.com/ps.mahmood.sabir/devops-practices/-/jobs/artifacts/master/download?job=build -O /apps/artifacts.zip


#Decompress 
cd /apps && unzip -o artifacts.zip


#Print current workdir
echo "Current working dir:"$(pwd)



# Options
#activeProfile=h2
serverPort=8070


echo "Whoami: "$(whoami)
echo "App will try to run on port: "$serverPort

# Run jar using H2 emmbeded DB
#java -jar -Dserver.port=${serverPort} -Dspring.profiles.active=${activeProfile} devops_sample/target/assignment-*.jar

# Run jar using mysql DB
java -jar -Dserver.port=${serverPort} devops_sample/target/assignment-*.jar



